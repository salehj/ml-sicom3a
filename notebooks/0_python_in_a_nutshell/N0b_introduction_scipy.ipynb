{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook can be run on mybinder: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fparcours-numerique-ia/master?filepath=notebooks%2F0_python_in_a_nutshell/N0b_introduction_scipy.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Numpy and Scipy\n",
    "\n",
    "Data analysis needs effective computational ressources to read/write and process data. Usually, the data set to be processed is a set of arrays. \n",
    "\n",
    "The main structure provided by [Numpy](https://numpy.org/) is the *Fixed-Type Arrays*: **ndarray**. It is an efficient way of storing data and processing them.\n",
    "\n",
    "\n",
    "[Scipy](https://www.scipy.org/) (*Scientific Python*) package is a dedicated tool that elaborates on Numpy to operate on *ndarray* efficiently. Quoting the *FAQ*, Scipy is \"*set of open source (BSD licensed) scientific and numerical tools for Python. It currently supports special functions, integration, ordinary differential equation (ODE) solvers, gradient optimization, parallel programming tools, an expression-to-C++ compiler for fast execution, and others. A good rule of thumb is that if it’s covered in a general textbook on numerical computing (for example, the well-known Numerical Recipes series), it’s probably implemented in scipy*\". This is the core of any data analysis package in Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Find the scipy and numpy module and define an alias in the local namespace\n",
    "import scipy as sp\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A = [0 1 2 3 4 5 6 7 8 9]\n",
      "B = [0 1 2 3 4 5 6 7 8 9]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "\u001b[0;31mDocstring:\u001b[0m\n",
       "array(object, dtype=None, *, copy=True, order='K', subok=False, ndmin=0,\n",
       "      like=None)\n",
       "\n",
       "Create an array.\n",
       "\n",
       "Parameters\n",
       "----------\n",
       "object : array_like\n",
       "    An array, any object exposing the array interface, an object whose\n",
       "    __array__ method returns an array, or any (nested) sequence.\n",
       "dtype : data-type, optional\n",
       "    The desired data-type for the array.  If not given, then the type will\n",
       "    be determined as the minimum type required to hold the objects in the\n",
       "    sequence.\n",
       "copy : bool, optional\n",
       "    If true (default), then the object is copied.  Otherwise, a copy will\n",
       "    only be made if __array__ returns a copy, if obj is a nested sequence,\n",
       "    or if a copy is needed to satisfy any of the other requirements\n",
       "    (`dtype`, `order`, etc.).\n",
       "order : {'K', 'A', 'C', 'F'}, optional\n",
       "    Specify the memory layout of the array. If object is not an array, the\n",
       "    newly created array will be in C order (row major) unless 'F' is\n",
       "    specified, in which case it will be in Fortran order (column major).\n",
       "    If object is an array the following holds.\n",
       "\n",
       "    ===== ========= ===================================================\n",
       "    order  no copy                     copy=True\n",
       "    ===== ========= ===================================================\n",
       "    'K'   unchanged F & C order preserved, otherwise most similar order\n",
       "    'A'   unchanged F order if input is F and not C, otherwise C order\n",
       "    'C'   C order   C order\n",
       "    'F'   F order   F order\n",
       "    ===== ========= ===================================================\n",
       "\n",
       "    When ``copy=False`` and a copy is made for other reasons, the result is\n",
       "    the same as if ``copy=True``, with some exceptions for 'A', see the\n",
       "    Notes section. The default order is 'K'.\n",
       "subok : bool, optional\n",
       "    If True, then sub-classes will be passed-through, otherwise\n",
       "    the returned array will be forced to be a base-class array (default).\n",
       "ndmin : int, optional\n",
       "    Specifies the minimum number of dimensions that the resulting\n",
       "    array should have.  Ones will be pre-pended to the shape as\n",
       "    needed to meet this requirement.\n",
       "like : array_like\n",
       "    Reference object to allow the creation of arrays which are not\n",
       "    NumPy arrays. If an array-like passed in as ``like`` supports\n",
       "    the ``__array_function__`` protocol, the result will be defined\n",
       "    by it. In this case, it ensures the creation of an array object\n",
       "    compatible with that passed in via this argument.\n",
       "\n",
       "    .. versionadded:: 1.20.0\n",
       "\n",
       "Returns\n",
       "-------\n",
       "out : ndarray\n",
       "    An array object satisfying the specified requirements.\n",
       "\n",
       "See Also\n",
       "--------\n",
       "empty_like : Return an empty array with shape and type of input.\n",
       "ones_like : Return an array of ones with shape and type of input.\n",
       "zeros_like : Return an array of zeros with shape and type of input.\n",
       "full_like : Return a new array with shape of input filled with value.\n",
       "empty : Return a new uninitialized array.\n",
       "ones : Return a new array setting values to one.\n",
       "zeros : Return a new array setting values to zero.\n",
       "full : Return a new array of given shape filled with value.\n",
       "\n",
       "\n",
       "Notes\n",
       "-----\n",
       "When order is 'A' and `object` is an array in neither 'C' nor 'F' order,\n",
       "and a copy is forced by a change in dtype, then the order of the result is\n",
       "not necessarily 'C' as expected. This is likely a bug.\n",
       "\n",
       "Examples\n",
       "--------\n",
       ">>> np.array([1, 2, 3])\n",
       "array([1, 2, 3])\n",
       "\n",
       "Upcasting:\n",
       "\n",
       ">>> np.array([1, 2, 3.0])\n",
       "array([ 1.,  2.,  3.])\n",
       "\n",
       "More than one dimension:\n",
       "\n",
       ">>> np.array([[1, 2], [3, 4]])\n",
       "array([[1, 2],\n",
       "       [3, 4]])\n",
       "\n",
       "Minimum dimensions 2:\n",
       "\n",
       ">>> np.array([1, 2, 3], ndmin=2)\n",
       "array([[1, 2, 3]])\n",
       "\n",
       "Type provided:\n",
       "\n",
       ">>> np.array([1, 2, 3], dtype=complex)\n",
       "array([ 1.+0.j,  2.+0.j,  3.+0.j])\n",
       "\n",
       "Data-type consisting of more than one element:\n",
       "\n",
       ">>> x = np.array([(1,2),(3,4)],dtype=[('a','<i4'),('b','<i4')])\n",
       ">>> x['a']\n",
       "array([1, 3])\n",
       "\n",
       "Creating an array from sub-classes:\n",
       "\n",
       ">>> np.array(np.mat('1 2; 3 4'))\n",
       "array([[1, 2],\n",
       "       [3, 4]])\n",
       "\n",
       ">>> np.array(np.mat('1 2; 3 4'), subok=True)\n",
       "matrix([[1, 2],\n",
       "        [3, 4]])\n",
       "\u001b[0;31mType:\u001b[0m      builtin_function_or_method\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "A = np.array(range(10)) # Create array from a list\n",
    "print(\"A = {}\".format(A)) # note that there is 10 elements: 0,1,...,9\n",
    "B = np.arange(10) # Create array from scratch\n",
    "print(\"B = {}\".format(B))\n",
    "np.array?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basics of Arrays \n",
    "\n",
    "There are plenty of functions to create and to initialize specific array (np.zeros, np.ones, np.empty ...). For each case, it is possible to define the type (int8, uint8, float64 ...) by providing the corresponding parameter. More information regarding the different array types can be found here: https://numpy.org/doc/stable/user/basics.types.html and https://numpy.org/doc/stable/reference/arrays.dtypes.html.\n",
    "\n",
    "### Getting attributes\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of elements in A: 10\n",
      "Number of dimension of A: 1\n",
      "Dimension of A: (10,)\n",
      "Type of element in A: int64\n"
     ]
    }
   ],
   "source": [
    "# Attributes\n",
    "print(\"Number of elements in A: {}\".format(A.size))\n",
    "print(\"Number of dimension of A: {}\".format(A.ndim))\n",
    "print(\"Dimension of A: {}\".format(A.shape))\n",
    "print(\"Type of element in A: {}\".format(A.dtype))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to modify explicitely some attributes, in particlar the *shape*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "B = \n",
      " [[0 1 2 3 4]\n",
      " [5 6 7 8 9]]\n",
      "(2, 5)\n",
      "(10,)\n"
     ]
    }
   ],
   "source": [
    "B.shape = (2,5) # Change the shape to two lines, 5 columns -> the number of total elements should be the same\n",
    "print(\"B = \\n {}\".format(B))\n",
    "C = B.reshape(10) # The function return a new array with the corresponding shape\n",
    "print(B.shape)\n",
    "print(C.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing elements"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A = [0 1 2 3 4 5 6 7 8 9]\n",
      "0\n",
      "1\n",
      "9\n",
      "8\n"
     ]
    }
   ],
   "source": [
    "print(\"A = {}\".format(A))\n",
    "print(A[0]) # First element\n",
    "print(A[1]) # Second element\n",
    "print(A[-1]) # Last element\n",
    "print(A[-2]) # Antepenultimate element"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0 1 2]\n",
      "[0 2 4 6 8]\n",
      "[7 8]\n"
     ]
    }
   ],
   "source": [
    "# Some slicing\n",
    "print(A[0:3]) # Return an array of elements of A from the first (index 0) to the third (index 2)\n",
    "print(A[::2]) # All elements with a step of 2\n",
    "print(A[-3:-1]) # Can use reverse order"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computation on Array\n",
    "### Universal functions\n",
    "A general comment for interpreted laguage: **do not use loop if you can** ! It is slow and inefficient.\n",
    "\n",
    "The comment apply here with Python. Scipy provide a large types of operation that are optimized to work on array directly (as in Matlab, R ...). In particular, *universal functions* (ufuncs) are a set of functions for fast element-wise operations (+, -, power ...). Let see a short example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "using loops\n",
      "29.6 ms ± 533 µs per loop (mean ± std. dev. of 7 runs, 10 loops each)\n",
      "using ufuncs\n",
      "42.9 µs ± 726 ns per loop (mean ± std. dev. of 7 runs, 10000 loops each)\n"
     ]
    }
   ],
   "source": [
    "def my_add(M,N): # Suppose that A and B have the same shape\n",
    "    P = np.empty_like(M)\n",
    "    nl, nr = M.shape\n",
    "    for i in range(nl):\n",
    "        for j in range(nr):\n",
    "            P[i,j] = M[i,j] + N[i,j]\n",
    "    return P\n",
    "\n",
    "M, N = np.arange(100000).reshape(1000,100), np.arange(100000).reshape(1000,100)\n",
    "\n",
    "# Evaluate execution time by repeating several runs based on a total of 2 seconds execution window\n",
    "print('using loops')\n",
    "%timeit my_add(M,N) # using loops\n",
    "print('using ufuncs')\n",
    "%timeit M + N # using ufuncs equivalent to sp.add(A,B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most all conventional functions exist: arithmetic, trigonometric, log/exp ... A detailed list is available here: https://numpy.org/doc/stable/reference/ufuncs.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reduction\n",
    "Numpy provides a set of functions to extrac values from the array itself and for some specific dimension of the array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A = \n",
      "[[0.33162385 0.12358859 0.97580111 0.92312367]\n",
      " [0.66819625 0.90663743 0.72297168 0.08685877]\n",
      " [0.16006682 0.47601541 0.70873727 0.9373805 ]\n",
      " [0.31654306 0.66572629 0.60949886 0.77855965]\n",
      " [0.10248447 0.60317889 0.13023542 0.68807504]]\n",
      "10.915303014004264\n",
      "[1.57891445 2.77514661 3.14724433 3.41399762]\n",
      "[2.35413721 2.38466413 2.2822     2.37032785 1.52397383]\n"
     ]
    }
   ],
   "source": [
    "A = sp.random.rand(5,4)\n",
    "print(\"A = \\n{}\".format(A))\n",
    "print(A.sum()) # Sum over all element\n",
    "print(A.sum(axis=0)) # Sum over the lines: return an array of values\n",
    "print(A.sum(axis=1)) # over the columns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the same convention, it is possible to get the cumulative sum (cumsum), product of element (prod, cumprod), the maximum/minimum value (max, min) and their position (argmax, argmin) and the first and second statistical moment (mean, var/std). It is also possible to check if a condition is fullfilled for all or any elements of the array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.any(A>0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.all(A>0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Some exercices\n",
    "- Find the maximum and minimum value of A\n",
    "- Find the maximum of each line\n",
    "- Find the mean value of each row\n",
    "- Find the position of the minimum value of each row"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.9758011062168064\n",
      "0.08685877200331249\n",
      "[0.66819625 0.90663743 0.97580111 0.9373805 ]\n",
      "[0.5885343  0.59616603 0.57055    0.59258196 0.38099346]\n",
      "[1 3 0 0 0]\n"
     ]
    }
   ],
   "source": [
    "print(A.max())\n",
    "print(A.min())\n",
    "print(A.max(axis=0))\n",
    "print(A.mean(axis=1))\n",
    "print(A.argmin(axis=1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Broadcasting\n",
    "Broadcasting allow to define efficient operations between arrays of different sizes, given some of them are compatible. An extreme example is adding a scalar to a matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[3.33162385, 3.12358859, 3.97580111, 3.92312367],\n",
       "       [3.66819625, 3.90663743, 3.72297168, 3.08685877],\n",
       "       [3.16006682, 3.47601541, 3.70873727, 3.9373805 ],\n",
       "       [3.31654306, 3.66572629, 3.60949886, 3.77855965],\n",
       "       [3.10248447, 3.60317889, 3.13023542, 3.68807504]])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A+3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Easy ? Now if I need to center the data, it is also super easy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Size of A: (5, 4)\n",
      "Size of the average of A along the lines: (4,)\n",
      "Size of centered A: (5, 4)\n",
      "Ac=\n",
      "[[ 0.01584096 -0.43144074  0.34635224  0.24032414]\n",
      " [ 0.35241336  0.35160811  0.09352281 -0.59594075]\n",
      " [-0.15571607 -0.07901391  0.0792884   0.25458097]\n",
      " [ 0.00076017  0.11069697 -0.01995001  0.09576012]\n",
      " [-0.21329842  0.04814957 -0.49921345  0.00527552]]\n"
     ]
    }
   ],
   "source": [
    "print('Size of A: {}'.format(A.shape))\n",
    "print('Size of the average of A along the lines: {}'.format(A.mean(axis=0).shape))\n",
    "# Suppose that each line is a sample, and each column a measurement (i.e., a variable)\n",
    "Ac= A - A.mean(axis=0) \n",
    "print('Size of centered A: {}'.format(Ac.shape))\n",
    "print ('Ac=\\n{}'.format(Ac))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we need to standardize the data (substract the mean and divide by the standard deviation), it can be achieved easily:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 0.08038085 -1.67789481  1.24880744  0.77011909]\n",
      " [ 1.78823046  1.36742169  0.33720581 -1.90969307]\n",
      " [-0.79014093 -0.30728909  0.28588221  0.81580512]\n",
      " [ 0.00385728  0.43050609 -0.07193173  0.30686346]\n",
      " [-1.08232766  0.18725611 -1.79996373  0.0169054 ]]\n"
     ]
    }
   ],
   "source": [
    "As = (A-A.mean(axis=0))/A.std(axis=0)\n",
    "print(As)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More details about broadcasting can be found here: https://docs.scipy.org/doc/numpy-1.13.0/user/basics.broadcasting.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ploting in Python\n",
    "The package [Matplotlib](https://matplotlib.org/) offers several functions to plot data. Below an example using 2D data, more complicated plots can be constructed when needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXcAAAD4CAYAAAAXUaZHAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8qNh9FAAAACXBIWXMAAAsTAAALEwEAmpwYAAAhj0lEQVR4nO3deXxU1f3/8dcnGyEJBAIk7HtkR5DIqhbE1q2ttlWLFkVEUGvVWmuLtt/a1tpqv35bbavfr1RRVDRaXLCW4oKkti4Ywr4TkSUQEgghZN/m/P7I6A8RhGQyuZmZ9/PxyGNm7syd+zlM8s7l5NxzzDmHiIiElyivCxARkeancBcRCUMKdxGRMKRwFxEJQwp3EZEwFON1AQCdO3d2ffv2bfL+5eXlJCYmNl9BrVyktRfU5kihNjdOTk7OQedcl+M91yrCvW/fvqxcubLJ+2dlZTF58uTmK6iVi7T2gtocKdTmxjGzXSd6Tt0yIiJhSOEuIhKGFO4iImFI4S4iEoYU7iIiYeik4W5m882s0Mw2HLUtxczeMrPt/tuORz13l5nlmtlWMzs/WIWLiMiJncqZ+1PABcdsmwssc86lA8v8jzGzocA0YJh/n0fNLLrZqhURkVNy0nB3zr0LHDpm8yXAAv/9BcClR23PdM5VO+c+AXKBsc1TqohIeFnw/k42FdUH5b2behFTmnMuH8A5l29mqf7tPYAPj3pdnn/bF5jZHGAOQFpaGllZWU0sBcrKygLaP9REWntBbY4UkdTmTUX1/Hd2FWd0cQwNQpub+wpVO862464G4pybB8wDyMjIcIFclRZpV7VFWntBbY4UkdLm/SVV3PGnfzMgNYnZI31BaXNTR8sUmFk3AP9toX97HtDrqNf1BPY1vTwRkfBSU+fj+wtzqKqt5/+mn0F8zPHOiQPX1HB/DZjhvz8DWHzU9mlm1sbM+gHpwEeBlSgiEj5+u2Qzq3Yf5oHLRjIwtV3QjnPSbhkzex6YDHQ2szzgHuB+4EUzmwXsBi4HcM5tNLMXgU1AHXCzcy44fy0QEQkxi9fs5an3d3LdpH58fWT3oB7rpOHunLvyBE9NPcHr7wPuC6QoEZFwszn/CD99aR1j+6Zw10WDg348XaEqIhJkJRW13PBMDsltY/nL90YTGx386G0V87mLiIQrn89x2wuryS+pJHPOBFLbxbfIcXXmLiISRA+9vY2srQe45xvDGNOn48l3aCYKdxGRIHlrUwF/eieXKzJ68r1xvVv02Ap3EZEg2HGgjB+9sIaRPZP59SXDMQvOePYTUbiLiDSzsuo6bngmh9iYKP53+hjiY1t+/kSFu4hIM3LO8ZNFa/n4QBl/uXI0PTq09aQOhbuISDOa9+4Olqzfz9wLBzNxYGfP6lC4i4g0k/dyD/LA0i1cPLIbs8/u72ktCncRkWaQV1zBD55bxcDUJH7/nZEt/gfUYyncRUQCVFFTx+ync6jzOR67OoPENt5fH6pwFxEJgHOOO/+2jq37j/DnK0fTr3Oi1yUBCncRkYA8sjyXf6zPZ+6Fg5k8KPXkO7QQhbuISBO9tamAB9/cxrdG9/D8D6jHUriLiDTBtoJSfpi5mpE9k/ndt0d4/gfUYyncRUQa6XBFDbOfXklCmxjmXZ3hyRWoJ6NwFxFphLp6Hz94bjX5h6t47OoxdE1umSl8G8v78ToiIiHkviWb+U/uQf77spGc0bvlpvBtLJ25i4icohdX7uHJ9xrWQL08o5fX5XwphbuIyCnI2VXMz1/ZwFkDO3N3C6yBGiiFu4jISewvqeLGZ3Po1iGev1w1mpgWWAM1UOpzFxH5EhU1dVz/dDYV1XUsvH4cHRLivC7plCjcRUROwOdz/OiFtWzad4THZ2RwWlo7r0s6Za3//xYiIh558M2tLN24n59dPJRzB6d5XU6jKNxFRI5jUU4ej2Z9zFXjenPdpL5el9NoCncRkWN89Mkh7np5HZMGduJX3xzW6qYWOBUKdxGRo+wuquCGZ1bSq2MCj141htgQGBlzPKFZtYhIEBypquW6Bdn4HDxx7ZkkJ8R6XVKTKdxFRGiYM+bmhavYebCc/5s+ptUsutFUGgopIgLc+/om/r39IPd/ewQTBnTyupyABXTmbma3m9lGM9tgZs+bWbyZpZjZW2a23X/bemfWEREBFry/kwUf7GL22f2YNra31+U0iyaHu5n1AG4FMpxzw4FoYBowF1jmnEsHlvkfi4i0SllbC/nV3zdy3pBU5l44xOtymk2gfe4xQFsziwESgH3AJcAC//MLgEsDPIaISFBs3FfCzQtXMbhrex6aNproqNAb8ngi5pxr+s5mtwH3AZXAm86575nZYedch6NeU+yc+0LXjJnNAeYApKWljcnMzGxyHWVlZSQlJTV5/1ATae0FtTlStGSbiyp93PthFVEG/zU+no7x3owvCaTNU6ZMyXHOZRz3Sedck76AjsA7QBcgFngVmA4cPuZ1xSd7rzFjxrhALF++PKD9Q02ktdc5tTlStFSbSypr3Pl//Jcb9oulbtO+khY55okE0mZgpTtBrgbyq+o84BPn3AHnXC3wMjARKDCzbgD+28IAjiEi0qxq/UMecwvL+N/pZzCkW3uvSwqKQMJ9NzDezBKs4drcqcBm4DVghv81M4DFgZUoItI8nHP87JX1/Hv7QX77rRGcnd7F65KCpsnj3J1zK8xsEbAKqANWA/OAJOBFM5tFwy+Ay5ujUBGRQD2yPJcXV+Zxy7kDueLM1r1MXqACuojJOXcPcM8xm6tpOIsXEWk1Xl29lwff3Ma3RvfgR189zetygk7TD4hI2Pvg4yLuXLSW8f1TuP87I0JylsfGUriLSFjLLSzlhmdW0qdTIo9Nz6BNTLTXJbUIhbuIhK3C0iqufTKbuJgongzxWR4bSxOHiUhYKq2q5dr52RSV1ZA5Zzy9UhK8LqlF6cxdRMJOdV09Nz6bw7aCUh6dfgan9+rgdUktTmfuIhJWfD7Hj/+2jvdyi/ify09nyqBUr0vyhM7cRSRsOOf4zT828/e1+/jpBYP5zpieXpfkGYW7iISNv/57B/Pf+4RrJ/blxq/097ocTyncRSQsvLI6j98u2cLFI7vxi68PjYix7F9G4S4iIe9f2w5w59/WMaF/J/5wxelEhdG87E2lcBeRkLYu7zA3PZtDelo7HrtmTMRcpHQyCncRCVk7D5Yz88lsOibEsWDmmbSPj5yLlE5G4S4iIanwSBXXzP8In3M8PWssqe3jvS6pVdE4dxEJOYcrarhm/kccLKtm4fXjGNAlspYjPBU6cxeRkFJRU8fMp7LZcaCceVdnMLr3F5ZoFhTuIhJCquvqueGZHNbuOcyfrhzFWemdvS6p1VK3jIiEhHqf4/YX1vDv7Qf5/WUjuWB4N69LatV05i4ird6na58uWb+fn188hCsywnuJvOagcBeRVu/+pVvIzN7DLecO5PqzI3tagVOlcBeRVu3RrFwe+9cOrpnQJyLWPm0uCncRabUWrtjF75du5ZJR3fnlN4ZF/HwxjaFwF5FW6e9r9/HzVzdw7uBUHrxc88U0lsJdRFqdtzYVcPsLazizbwqPfu8MYqMVVY2lfzERaVX+te0ANy9cxfAeyTwxI4P4WE0E1hQa5y4ircbmonoeenslA1OTWDBzLO00EViT6cxdRFqFnF2HeGhVFb1TEnhm1liSExTsgVC4i4jn1ueVcO38bDq0MRbOHkenpDZelxTy1C0jIp7anH+Eq+evIDkhlttHxpLaTlP3NgeduYuIZ3ILy5j++AriY6J5fvZ4OrVVJDUX/UuKiCd2FZXzvcc/xMx4bvY4eqUkeF1SWFG4i0iLyyuu4Kq/rqCmzsfC68fRX4ttNLuAwt3MOpjZIjPbYmabzWyCmaWY2Vtmtt1/q5n0ReQzecUVXPnXDzlSVcszs8YxqGs7r0sKS4GeuT8MLHXODQZOBzYDc4Flzrl0YJn/sYjIZ8F+uKKWhdePY3iPZK9LCltNDnczaw+cAzwB4Jyrcc4dBi4BFvhftgC4NLASRSQc7D1c+blgH9mzg9clhTVzzjVtR7NRwDxgEw1n7TnAbcBe51yHo15X7Jz7QteMmc0B5gCkpaWNyczMbFIdAGVlZSQlRU6fXaS1F9TmUFdU6eP+j6ooq3X85Mx4+iUff0qBcGrzqQqkzVOmTMlxzmUc90nnXJO+gAygDhjnf/wwcC9w+JjXFZ/svcaMGeMCsXz58oD2DzWR1l7n1OZQlldc4c56YJkbfs9St3ZP8Ze+Nlza3BiBtBlY6U6Qq4H0uecBec65Ff7Hi4AzgAIz6wbgvy0M4BgiEsL2Hq5k2rwPOFxRy7Oz1BXTkpoc7s65/cAeMxvk3zSVhi6a14AZ/m0zgMUBVSgiIWnv4UqunPfhZ8F+eq8OXpcUUQKdfuAWYKGZxQE7gJk0/MJ40cxmAbuBywM8hoiEmE+DvbiiRsHukYDC3Tm3hoa+92NNDeR9RSR07TlUwVWP64zda5o4TESazScHy/neXz+kvKZewe4xhbuINIvtBaVc9fgKfD7H87PHM7R7e69LimgKdxEJ2MZ9JVz9xEfERBmZc8aTnqYpBbymicNEJCBr9xzmynkfEh8TxQs3TFCwtxI6cxeRJsveeYiZT2aTkhjHwus1bW9ronAXkSZ5P/cgsxaspFtyPM/NHk/XZK2g1JqoW0ZEGm351kJmPpVN75QEXrhhgoK9FdKZu4g0ytIN+7nl+VWcltaOZ2aNIyUxzuuS5Dh05i4ip+zF7D18f2EOw3sk89zs8Qr2Vkxn7iJySv767g7uW7KZs9M789jVY0iIU3y0Zvp0RORLOed48M2tPLL8Yy4e0Y0/fPd02sQcfz52aT0U7iJyQvU+xy8Wb2Dhit1cObYXv7l0BNFR5nVZcgoU7iJyXDV1Pn704hpeX5fPjV8ZwE8vGISZgj1UKNxF5Asqa+q58dkc/rXtAHMvHMyNXxngdUnSSAp3EfmckspaZj2Vzardxdz/7RFMG9vb65KkCRTuIvKZgiNVzJj/ER8fKOMvV53BRSO6eV2SNJHCXUQAyC0sZcb8bIoranhixpmcc1oXr0uSACjcRYScXYe47qmVxEYbL8yZwIieyV6XJAFSuItEuDc27ufW51fTvUNbFswcS+9OmtkxHCjcRSLYsx/u4heLNzCiZwfmz8igU1Ibr0uSZqJwF4lAzjn+581t/GV5LlMHp/Lnq0ZrOoEwo09TJMLU1vu46+X1LMrJY9qZvfjNpcOJidYcguFG4S4SQcqq6/jBc6vI2nqAH56Xzm1T03XVaZhSuItEiPySSq57aiXbCkr53bdHcKUuTgprCneRCLBhbwmzFmRTXl3PEzMymDwo1euSJMgU7iJh7u1NBdyauZoObWNZdNMEBndt73VJ0gIU7iJh7Mn3PuHe1zcxrHsyT8zIILW91jqNFAp3kTBU73Pc+/omnnp/J18bmsZD00ZpqGOE0actEmbKq+u45fnVvLOlkNln92PuhUO0wEYEUriLhJH8kkpmPbWSrQWl3HvpcK4e38frksQjCneRMLFqdzFzns6hqlYjYgQCvizNzKLNbLWZve5/nGJmb5nZdv9tx8DLFJEv81JOHtMe+5CEuGhe/v5EBbsEHu7AbcDmox7PBZY559KBZf7HIhIE9T7Hb5ds5o6/rWVMn44svnkSp6W187osaQUCCncz6wlcDDx+1OZLgAX++wuASwM5hogc35GqWmYtyGbeuzu4ZkIfnp41lo6JcV6XJa2EOeeavrPZIuB3QDvgx865r5vZYedch6NeU+yc+0LXjJnNAeYApKWljcnMzGxyHWVlZSQlJTV5/1ATae0FtflY+8t9PLyqisIKx/QhcUzpHdvC1QWHPufGmTJlSo5zLuN4zzX5D6pm9nWg0DmXY2aTG7u/c24eMA8gIyPDTZ7c6Lf4TFZWFoHsH2oirb2gNh/tP9sP8rvnVhFlMSycPYbx/Tu1fHFBos+5+QQyWmYS8E0zuwiIB9qb2bNAgZl1c87lm1k3oLA5ChWJdM45nnxvJ/ct2Ux6ahJ/vSaDXilaNUmOr8l97s65u5xzPZ1zfYFpwDvOuenAa8AM/8tmAIsDrlIkwlXU1PHDF9bw69c3MXVwKotumqhgly8VjHHu9wMvmtksYDdweRCOIRIxdh4s58Znc9haUMqd5w/ipq8MIEpXnMpJNEu4O+eygCz//SJganO8r0ike2dLAbdlriE6ylgwcyznnNbF65IkROgKVZFWyOdzvJpbw6tLVzKse3v+b/oYdcNIoyjcRVqZkopabn9xDe/k1nLZmJ785tLhxMdGe12WhBiFu0grsjn/CDc8k0N+SSXXDI3jV5eN1Bqn0iRa8lykFXDO8WL2Hr716HtU19WTOWcC5/aOVbBLk+nMXcRjFTV1/PyVDby8ei+TBnbioe+Opku7NmR94nVlEsoU7iIe2lZQyvcXruLjA2Xcft5p/ODcgVpYQ5qFwl3EI39buYf/WryBpDaxLJw1jokDO3tdkoQRhbtIC6usqee/Fm9gUU4eE/p34uErR5HaTgtXS/NSuIu0oNzChm6Y7YVl3Do1ndumpqsbRoJC4S7SApxzPP/RHn79+kYS42J4+rqxnJ2uq00leBTuIkF2uKKGuS+tZ+nG/Zyd3pn/ufx0UturG0aCS+EuEkQffFzE7S+soai8mp9dNIRZZ/XTpF/SIhTuIkFQW+/jobe38WjWx/TtlMjL10xiRM9kr8uSCKJwF2lmu4rKuTVzDWv3HOa7Gb34xTeGkthGP2rSsvQdJ9JMnHO8vGov97y2kSiDR646g4tHdvO6LIlQCneRZlBUVs3dr6znjY0FjO2bwh+njaJHh7ZelyURTOEuEqA3Nu7n7pfXU1pVx90XDWbWWf01dl08p3AXaaIjVbX86rVNvLQqj2Hd2/Pc7FEM6trO67JEAIW7SJO8n3uQH/9tLQWl1dx67kB+cG46cTGaQVtaD4W7SCNU1tTzwNItPPX+Tvp3SeSlmyYyqlcHr8sS+QKFu8gp+nBHEXNfWsfOogpmTurLT84fTNs4LX8nrZPCXeQkSqtquf+fW1i4Yje9UxJ4bvY4Jg7Q9LzSuincRb7E8i2F3P3KegqOVHH9Wf2442uDdLYuIUHhLnIcxeU1/Pr1Tbyyei/pqUk8etNERvfu6HVZIqdM4S5yFOcc/1ifzz2LN1JSWcut5w7k5nMH0iZGZ+sSWhTuIn55xRX88rVNvL25gBE9knlm1jiGdm/vdVkiTaJwl4hXW+9j/n8+4aG3twNw14WDmXVWP2KiNW5dQpfCXSLayp2H+NkrG9haUMp5Q1L55TeH0bNjgtdliQRM4S4Rqbi8hgeWbiEzew/dk+OZd/UYvjasq9dliTQbhbtEFOccL63ay2+XbKakspY55/Tntqnpmm9dwk6Tv6PNrBfwNNAV8AHznHMPm1kK8ALQF9gJXOGcKw68VJHAbNhbwq/+vpHsncWc0bsD931rBEO66Q+mEp4COV2pA+5wzq0ys3ZAjpm9BVwLLHPO3W9mc4G5wE8DL1WkaYrKqnnwzW1kZu8mJSGO+789gisyemktUwlrTQ5351w+kO+/X2pmm4EewCXAZP/LFgBZKNzFA7X1Pp79cBd/fGsb5TX1zJzYj9vOSye5bazXpYkEnTnnAn8Ts77Au8BwYLdzrsNRzxU7575waZ+ZzQHmAKSlpY3JzMxs8vHLyspISkpq8v6hJtLaC41v86aiehZurmZvmWNYpyiuGtKGHkmhNbRRn3NkCKTNU6ZMyXHOZRz3SedcQF9AEpADfNv/+PAxzxef7D3GjBnjArF8+fKA9g81kdZe5069zZ8cKHM3PL3S9fnp6+6sB5a5pRvync/nC25xQaLPOTIE0mZgpTtBrgY0RMDMYoGXgIXOuZf9mwvMrJtzLt/MugGFgRxD5FQcKq/hT8u2s3DFLmKjo7jjq6cx+5z+xMdq2gCJTIGMljHgCWCzc+4PRz31GjADuN9/uzigCkW+RFVtPU+9v5NHludSXl3Hd8/sze3npZPaPt7r0kQ8FciZ+yTgamC9ma3xb7ubhlB/0cxmAbuBywOqUOQ4fD7H4rV7efCNbew9XMm5g1OZe+FgTkvTGqYiENhomf8AJxpLNrWp7ytyMu/lHuR3/9zMhr1HGNa9Pf992UgmDtTiGSJH02V5EjJydhXz4Btb+WBHEd2T4/njd0/nktN7aLy6yHEo3KXV27ivhD/mVLF26ft0TorjF18fylXjeuuPpSJfQuEurVZuYRl/fGsb/1ifT0IM3Hn+IK6d2FfzwIicAv2USKuzq6icP7+Ty8ur8mgbG80t5w5ksO3j4ikDvS5NJGQo3KXVyC0s5ZHlH7N4zV5io6O4blI/bpo8gE5JbcjKyve6PJGQonAXz23OP8Jf3sllyYZ84mOimXVWP2af05/UdhqrLtJUCnfxzLq8w/z5nVze2lRAUpsYbvrKAGad1Y9OSW28Lk0k5CncpUU553j/4yIee3cH7247QPv4GH54XjozJ/YjOUGzNYo0F4W7tIjaeh9L1ucz790dbNx3hM5Jbbjz/EFcM6EP7eIV6iLNTeEuQVVWXUfmR7uZ/59P2FdSxYAuiTzwnRFcMqqHxqmLBJHCXYIiv6SSp97fyXMrdlNaVce4fince+lwpgxK1RWlIi1A4S7NxjnHhzsO8fQHO3lzUwHOOS4c0Y3ZZ/dnVK8OXpcnElEU7hKw8uo6Xlm9l6c/2Mm2gjI6JMRy/dn9mD6uD71SErwuTyQiKdylyT45WM7TH+xk0co8SqvrGNa9Pb+/bCTfPL27+tNFPKZwl0apqq3njY37yfxoDx/sKCImyrhoRDdmTOzLGb070LCGi4h4TeEup2TTviO8kL2bV9fso6Syll4pbbnjq6fx3TN7adUjkVZI4S4nVFJZy+vr9vFC9h7W5ZUQFx3FBcO7Mu3MXozv30mjXkRaMYW7fE51XT1ZWw/w6uq9LNtSSE2dj8Fd23HPN4Zy6agedEyM87pEETkFCnfB53Pk7C7mldV7+ce6fEoqa+mUGMdVY3vzrdE9GNkzWX3pIiFG4R6hnHNs3HeEf6zP5+9r95FXXEnb2GjOH5bGpaN7cNbAzsRER3ldpog0kcI9gjjnWLPnMP/csJ9/bshnz6FKoqOMSQM7c8fXTuNrQ7tqlSORMKGf5DBX73Os3l3MkvX7Wbohn30lVcRGG2cN7MwtU9L56tA09aOLhCGFexgqqazl3W0HeGdLIVlbCymuqCUuJopz0rvw4/MHMXVIGsltNROjSDhTuIcB5xwfHyhn+ZZClm0pIHtnMfU+R8eEWKYMSmXK4FQmD+qiqXVFIojCPUQdKK3m/Y8P8l7uQd7LLWLv4UoABndtxw3n9GfqkFRG9epItMaii0QkhXuIKK+u46Odh3hv+0HeWFvJnqVvA9A+PoaJAzpz4+QBTBnUhZ4dNVGXiCjcW63CI1Ws3FVM9s5DrNxZzKb8I9T7HHHRUQxIhjvPH8RZAzszvEeyzs5F5AsU7q1ATZ2PbQWlrMsrIWdXMSt3HWJXUQUA8bFRjOrVge9PHsDYfilk9Elhxfv/ZvLkgR5XLSKtmcK9hdXV+9heWMb6vBLW7T3M+rwSNueXUlPvA6BTYhwZfTty9fg+ZPRNYVj39sTqYiIRaSSFe5A459h/pIqt+0vZVlDK1v1lbCtouF9d1xDk7drEMLxHMjMn9WV4j2RG9kymd0qCLvUXkYAp3ANUVl3HrqJydhVVsLOonF0HK/j4QBlbC0oprar77HWp7dowqGs7po/vw8ieyYzokUzfTomaWVFEgiJo4W5mFwAPA9HA4865+4N1rGDx+RyHKmrYX1JFfkkV+0sqyfff33Oogp1FFRwsq/7cPl3ataFfp0QuHdWD07q2Y1BaO05LS6JDgq4CFZGWE5RwN7No4BHgq0AekG1mrznnNgXjeF/GOUdNvY+K6nrKa+qoqKmnvLrhtrSqlkPltRRX1HCovIbi8hoOVdRQXFFLUVk1hUeqP+sL/1RMlJHWPp5eKW05b0gqfTol0rdTAn06JdK7UwJJmptFRFqBYCXRWCDXObcDwMwygUuAZg33LfuPcMtzqzlSVkHcR+9QX++o8znqfQ23dfU+qup81PvcSd8rMS6ajolxpCTG0TEhjv6dE0lrH0+35Hi6Jv//286JbdSVIiKtXrDCvQew56jHecC4o19gZnOAOQBpaWlkZWU1+iCFFT6SrYb2CT7i4mqIMSPKINogyiDKjLjoGOKjoU2Mff422mgbA+3ijMRYIy7608Cu8381DEWkBjgAhw7AoUZXGBxlZWVN+vcKZWpzZFCbm0+wwv14p7afO312zs0D5gFkZGS4yZMnN+lAV1wEWVlZNHX/UBRp7QW1OVKozc0nWAOo84BeRz3uCewL0rFEROQYwQr3bCDdzPqZWRwwDXgtSMcSEZFjBKVbxjlXZ2Y/AN6gYSjkfOfcxmAcS0REviho4/acc0uAJcF6fxEROTFNWiIiEoYU7iIiYUjhLiIShhTuIiJhyJw7+aX5QS/C7ACwK4C36AwcbKZyQkGktRfU5kihNjdOH+dcl+M90SrCPVBmttI5l+F1HS0l0toLanOkUJubj7plRETCkMJdRCQMhUu4z/O6gBYWae0FtTlSqM3NJCz63EVE5PPC5cxdRESOonAXEQlDIR3uZnaBmW01s1wzm+t1PcFmZr3MbLmZbTazjWZ2m9c1tRQzizaz1Wb2ute1tAQz62Bmi8xsi//znuB1TcFmZrf7v683mNnzZhbvdU3Nzczmm1mhmW04aluKmb1lZtv9tx2b41ghG+5HLcJ9ITAUuNLMhnpbVdDVAXc454YA44GbI6DNn7oN2Ox1ES3oYWCpc24wcDph3nYz6wHcCmQ454bTMFX4NG+rCoqngAuO2TYXWOacSweW+R8HLGTDnaMW4XbO1QCfLsIdtpxz+c65Vf77pTT8wPfwtqrgM7OewMXA417X0hLMrD1wDvAEgHOuxjl32NOiWkYM0NbMYoAEwnD1Nufcu3xxOeZLgAX++wuAS5vjWKEc7sdbhDvsg+5TZtYXGA2s8LiUlvAQ8BPA53EdLaU/cAB40t8V9biZJXpdVDA55/YCDwK7gXygxDn3prdVtZg051w+NJzAAanN8aahHO4nXYQ7XJlZEvAS8EPn3BGv6wkmM/s6UOicy/G6lhYUA5wB/K9zbjRQTjP9V7218vczXwL0A7oDiWY23duqQlsoh3tELsJtZrE0BPtC59zLXtfTAiYB3zSznTR0vZ1rZs96W1LQ5QF5zrlP/1e2iIawD2fnAZ845w4452qBl4GJHtfUUgrMrBuA/7awOd40lMM94hbhNjOjoR92s3PuD17X0xKcc3c553o65/rS8Bm/45wL6zM659x+YI+ZDfJvmgps8rCklrAbGG9mCf7v86mE+R+Rj/IaMMN/fwawuDneNGhrqAZbhC7CPQm4GlhvZmv82+72r1cr4eUWYKH/xGUHMNPjeoLKObfCzBYBq2gYFbaaMJyKwMyeByYDnc0sD7gHuB940cxm0fBL7vJmOZamHxARCT+h3C0jIiInoHAXEQlDCncRkTCkcBcRCUMKdxGRMKRwFxEJQwp3EZEw9P8ANv+aaU5tO3UAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "x = np.arange(0,10,0.01)\n",
    "y = x**2\n",
    "plt.plot(x,y)\n",
    "plt.grid()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:calc]",
   "language": "python",
   "name": "conda-env-calc-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
