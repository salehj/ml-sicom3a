# Lab 5 statement

The objective of this lab is to illustrate support vector machine for both linear and non-linear classification tasks. This also introduces
one-class SVM, an extension of the SVM principle to the novelty detection for unuspervised learning.

_Note: For each notebook, read the cells and run the code, then follow the instructions/questions in the `Questions` or `Exercise` cells._

See the notebooks in the [6_support_vector_machine ](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/6_support_vector_machine/) folder:


1. Run and plot the maximum margin separating hyperplane within a two-class separable or not dataset and interpret the role of the kernel and SVM parameters [`N1_plot_svm.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/6_support_vector_machine/N1_plot_svm.ipynb).

2. Perform SVM classification for some pairs of (zip code) digits that are the hardest to discriminate using a linear discriminant analysis method [`N2_svm_zip_digits.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/6_support_vector_machine/N2_svm_zip_digits.ipynb).

3. Train and plot one-class SVM algorithm to classify new data as *similar* or *different* to the training set on a toy problem
 [`N3_oneclasssvm_novelty_detection.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/6_support_vector_machine/N3_oneclasssvm_novelty_detection.ipynb).

The next notebooks are **optional**. But of course you can take a look at it during or after the class session!

4. *Optional*: Experiment the importance of scaling on a toy example for both linear and non-linear SVMs [`N4_importance_of_scaling-svm.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/6_support_vector_machine/N4_importance_of_scaling-svm.ipynb)

5. *Optional*:  Apply a simple dimension reduction, here PCA, to improve  the performance while reducing the computational burden of the SVM classifier on real faces data [`N5_svm_face_recognition.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/6_support_vector_machine/N5_svm_face_recognition.ipynb).

